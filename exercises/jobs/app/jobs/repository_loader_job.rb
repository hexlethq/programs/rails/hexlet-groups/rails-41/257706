class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(repository_id)
    repository = Repository.find(repository_id)

    repository.start_fetch

    begin
      info = FetchRepositoryInfo.call(repository.link)
      repository.success_fetch
      repository.update(info)
    rescue
      repository.fail_fetch!
    end
  end
end
