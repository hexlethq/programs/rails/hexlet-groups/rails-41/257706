# frozen_string_literal: true

class Repository < ApplicationRecord
  include AASM

  validates :link, presence: true, uniqueness: true

  # BEGIN
  aasm do
    state :created, initial: true
    state :fetching
    state :fetched
    state :failed

    event :start_fetch do
      transitions from: [:created, :fetched], to: :fetching
    end

    event :success_fetch do
      transitions from: :fetching, to: :fetched
    end

    event :fail_fetch do
      transitions from: :fetching, to: :failed
    end
  end
  # END
end
