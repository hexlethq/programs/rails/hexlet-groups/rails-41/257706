# frozen_string_literal: true

class FetchRepositoryInfo < ApplicationService
  def initialize(url)
    @url = url
  end

  def repository_name
    URI(@url).path[1..-1]
  end

  def call
    client = Octokit::Client.new
    repo = client.repo repository_name

    {
      owner_name: repo[:owner][:login],
      repo_name: repo[:name],
      description: repo[:description],
      default_branch: repo[:default_branch],
      watchers_count: repo[:watchers_count],
      language: repo[:language],
      repo_created_at: repo[:created_at],
      repo_updated_at: repo[:updated_at],
      link: @url
    }
  end
end
