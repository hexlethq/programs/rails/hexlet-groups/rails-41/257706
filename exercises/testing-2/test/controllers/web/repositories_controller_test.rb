# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  setup do
    json = File.read("#{Rails.root}/test/fixtures/files/response.json")

    stub_request(:get, 'https://api.github.com/repos/bblimke/webmock')
      .to_return(status: 200, body: json, headers: {
                    'Content-Type' => 'application/json'
                  })
  end

  test 'should create repository' do
    get new_repository_url

    assert_difference('Repository.count', 1) do
      post repositories_path, params: { repository: { link: 'https://github.com/bblimke/webmock' } }
    end
    assert_response :redirect

    follow_redirect!
    assert_response :success
  end
  # END
end
