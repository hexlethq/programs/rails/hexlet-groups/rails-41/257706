# frozen_string_literal: true

require 'application_system_test_case'

class PostsTest < ApplicationSystemTestCase
  test 'visiting the posts' do
    visit posts_url

    assert_selector 'h1', text: 'Posts'
  end

  test 'create new post' do
    visit posts_url

    click_on 'New Post'

    fill_in 'Title', with: 'title'
    fill_in 'Body', with: 'some body'

    click_on 'Create Post'
    assert_text 'Post was successfully created.'
  end

  test 'edit post' do
    visit posts_url

    click_on 'Edit', match: :first

    fill_in 'Title', with: 'title'
    fill_in 'Body', with: 'some body'

    click_on 'Update Post'
    assert_text 'Post was successfully updated.'
  end

  test 'delete post' do
    visit posts_url

    accept_alert do
      click_on 'Destroy', match: :first
    end

    assert_text 'Post was successfully destroyed.'
  end

  test 'create comment' do
    visit posts_url

    click_on 'Show', match: :first

    fill_in 'post_comment_body', with: 'some comment text'

    click_on 'Create Comment'
    assert_text 'Comment was successfully created.'
  end
end
