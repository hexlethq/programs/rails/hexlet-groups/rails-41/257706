# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :set_post
    before_action :set_comment, only: %i[edit update destroy]

    def edit; end

    def create
      @comment = @post.comments.build(comment_params)

      if @comment.save
        redirect_to @post, notice: t('.success')
      else
        redirect_to @post
      end
    end

    def update
      if @comment.update(comment_params)
        redirect_to @post, notice: t('.success')
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @comment.destroy

      redirect_to @post
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:post_id])
    end

    def set_comment
      @comment = Post::Comment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def comment_params
      params.require(:post_comment).permit(:body)
    end
  end
end
