# frozen_string_literal: true

class Post < ApplicationRecord
  has_many :comments, dependent: :delete_all

  validates :title, presence: true
  validates :body, length: { maximum: 500 }
end
