# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      host = 'https://rails-l4-collective-blog.herokuapp.com'
      sign_up_url = "#{host}/users/sign_up"
      users_url = "#{host}/users"

      response = URI.open(sign_up_url)

      html = response.read
      html_doc = Nokogiri::HTML(html)

      cookie = response.meta['set-cookie']
      token = html_doc.at('input[name="authenticity_token"]')['value']

      data = {
        authenticity_token: token,
        email: email,
        password: password,
        password_confirmation: password
      }

      headers = {
        'Cookie' => cookie
      }

      post_response = Net::HTTP.post(URI(users_url), data.to_query, headers)

      abort 'Error' unless post_response.code.to_i == 302

      post_response
      # END
    end
  end
end
