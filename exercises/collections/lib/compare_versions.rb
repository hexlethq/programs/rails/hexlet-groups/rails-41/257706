# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  major1, minor1 = version1.split('.').map(&:to_i)
  major2, minor2 = version2.split('.').map(&:to_i)

  is_equal = major1 == major2 && minor1 == minor2
  v1_greater = major1 > major2 || minor1 > minor2

  return 0 if is_equal
  return 1 if v1_greater

  -1
end
# END
