# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  keys = params.keys.sort

  result = []
  keys.each do |key|
    value = "#{key}=#{params.fetch(key)}"
    result << value
  end

  result.join('&')
end
# END
