# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  words = text.split

  censored = []
  words.each do |word|
    censored << (stop_words.include?(word) ? '$#%!' : word)
  end

  censored.join(' ')
  # END
end
