# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < Web::Movies::ApplicationController
      def index
        @comments = resource_movie.comments
      end

      def new
        @comment = resource_movie.comments.new
      end

      def create
        @comment = resource_movie.comments.create(comment_params)

        if @comment.save
          redirect_to movie_comments_url(resource_movie)
        else
          render :new
        end
      end

      def edit
        @comment = resource_movie.comments.find_by(id: params[:id])
      end

      def update
        @comment = resource_movie.comments.find_by(id: params[:id])

        if @comment.update(comment_params)
          redirect_to movie_comments_url(resource_movie)
        else
          render :edit
        end
      end

      def destroy
        @comment = resource_movie.comments.find_by(id: params[:id])

        if @comment.destroy
          redirect_to movie_comments_url(resource_movie), notice: 'success'
        else
          redirect_to movie_comments_url(resource_movie), notice: 'fail'
        end
      end

      private

      def comment_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
