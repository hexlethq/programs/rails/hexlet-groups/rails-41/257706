# frozen_string_literal: true

module Web
  module Movies
    class ReviewsController < Web::Movies::ApplicationController
      def index
        @reviews = resource_movie.reviews
      end

      def new
        @review = resource_movie.reviews.new
      end

      def create
        @review = resource_movie.reviews.create(review_params)

        if @review.save
          redirect_to movie_reviews_url(resource_movie)
        else
          render :new
        end
      end

      def edit
        @review = resource_movie.reviews.find_by(id: params[:id])
      end

      def update
        @review = resource_movie.reviews.find_by(id: params[:id])

        if @review.update(review_params)
          redirect_to movie_reviews_url(resource_movie)
        else
          render :edit
        end
      end

      def destroy
        @review = resource_movie.reviews.find_by(id: params[:id])

        if @review.destroy
          redirect_to movie_reviews_url(resource_movie), notice: 'success'
        else
          redirect_to movie_reviews_url(resource_movie), notice: 'fail'
        end
      end

      private

      def review_params
        params.require(:review).permit(:body)
      end
    end
  end
end
