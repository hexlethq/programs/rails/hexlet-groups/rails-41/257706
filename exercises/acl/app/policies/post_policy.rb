# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def index?
    true
  end

  def show?
    true
  end

  def create?
    user
  end

  def new?
    user
  end

  def update?
    author? || admin?
  end

  def edit?
    author? || admin?
  end

  def destroy?
    admin?
  end

  def admin?
    user&.admin?
  end

  def author?
    record&.author == user
  end
end
