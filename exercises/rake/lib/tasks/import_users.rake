# frozen_string_literal: true

require 'csv'

namespace :hexlet do
  desc 'This task import users'

  task :import_users, [:path] => [:environment] do |_task, args|
    abort 'Path is empty' unless args.path
    abort "File '#{args.path}' not exist" unless File.file?(args.path)

    file = File.read(args.path)

    users_table = CSV.parse(file, headers: true)

    users_table.each do |user|
      u = User.new(
        first_name: user['first_name'],
        last_name: user['last_name'],
        birthday: user['birthday'],
        email: user['email']
      )

      u.save!
    end
  end
end
