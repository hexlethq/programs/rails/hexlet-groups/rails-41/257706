# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bulletin1 = bulletins(:bulletin1)
    @bulletin2 = bulletins(:bulletin2)
  end

  test 'should get index' do
    get bulletins_url

    assert_select 'p', @bulletin1.title
    assert_select 'p', @bulletin2.title

    assert_response :success
  end

  test 'should open one bulletin published page' do
    get bulletin_path(@bulletin1)

    assert_response :success
    assert_select 'h1', @bulletin1.title
    assert_select 'b', 'Published'
    assert_select 'p', @bulletin1.body
  end

  test 'should open one bulletin not published page' do
    get bulletin_path(@bulletin2)

    assert_response :success
    assert_select 'h1', @bulletin2.title
    assert_select 'b', 'Not Published'
    assert_select 'p', @bulletin2.body
  end
end
