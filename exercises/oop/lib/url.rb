# frozen_string_literal: true

require 'forwardable'
require 'uri'

# BEGIN
class Url
  include Comparable
  extend Forwardable

  attr_reader :uri

  def_delegator :@uri, :scheme
  def_delegator :@uri, :host

  def initialize(string)
    @uri = URI(string)
    @params = query_string_to_hash(@uri.query || '')
  end

  def query_string_to_hash(query_string)
    query_string.split('&').each_with_object({}) do |param, acc|
      key, value = param.split('=')
      acc[key.to_sym] = value
    end
  end

  def query_params
    @params
  end

  def query_param(name, default = nil)
    @params.fetch(name, default)
  end

  def <=>(other)
    if @uri.to_s == other.uri.to_s
      0
    elsif @uri.to_s.size > other.uri.to_s.size
      1
    else
      -1
    end
  end
end
# END
