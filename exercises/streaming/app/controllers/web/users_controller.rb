# frozen_string_literal: true

require 'csv'

class Web::UsersController < Web::ApplicationController
  def index
    @users = User.page(params[:page])
  end

  def show
    @user = User.find params[:id]
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find params[:id]
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user, notice: t('success')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    @user = User.find params[:id]

    if @user.update(user_params)
      redirect_to @user, notice: t('success')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find params[:id]

    @user.destroy

    redirect_to users_url, notice: t('success')
  end

  def normal_csv
    respond_to do |format|
      format.csv do
        csv = generate_csv(User.column_names, User.all)
        send_data(csv)
      end
    end
  end

  # BEGIN
  def render_streaming_csv(csv_enumerator)
    # Delete this header so that Rack knows to stream the content.
    headers.delete("Content-Length")
    # Do not cache results from this action.
    headers["Cache-Control"] = "no-cache"
    # Let the browser know that this file is a CSV.
    headers['Content-Type'] = 'text/csv'
    # Do not buffer the result when using proxy servers.
    headers['X-Accel-Buffering'] = 'no'
    # Set the filename
    headers['Content-Disposition'] = "attachment; filename=\"report.csv\"" 
    # Set the response body as the enumerator
    self.response_body = csv_enumerator
  end

  def stream_csv
    respond_to do |format|
      format.csv { render_streaming_csv(csv_enumerator(User.column_names, User.all)) }
    end
  end
  # END

  private

  def generate_csv(column_names, records)
    CSV.generate do |csv|
      csv << column_names # add headers to the CSV

      records.find_each do |record|
        csv << record.attributes.values_at(*column_names)
      end
    end
  end

  # BEGIN
  def csv_enumerator(column_names, records)
    Enumerator.new do |csv|
      csv << column_names.to_csv # add headers to the CSV
      records.each do |record|
        csv << record.attributes.values_at(*column_names).to_csv
      end
    end
  end
  # END

  def user_params
    params.require(:user).permit(
      :name,
      :email
    )
  end
end

