# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  men = users.select { |user| user[:gender] == 'male' }

  result = {}
  men.each do |man|
    year = Time.new(man[:birthday]).year.to_s
    current = result[year] || 0
    result.store(year, current + 1)
  end

  result
end
# END
