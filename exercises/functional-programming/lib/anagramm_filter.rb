# frozen_string_literal: true

# BEGIN
def anagramm_filter(anagramm, list)
  initial = Set.new(anagramm.chars)

  list.select do |word|
    initial == Set.new(word.chars)
  end
end
# END
