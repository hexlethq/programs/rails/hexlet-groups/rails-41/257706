# frozen_string_literal: true

# BEGIN
def get_same_parity(numbers)
  first, _rest = numbers

  numbers.select { |number| first.even? == number.even? }
end
# END
