# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def test_empty_stack_size
    stack = Stack.new
    assert stack.size.zero?
  end

  def test_stack_size
    stack = Stack.new %w[ruby js lisp]
    assert_equal stack.size, 3
  end

  def test_stack_clear
    stack = Stack.new %w[ruby js lisp]
    stack.clear!
    assert stack.empty?
  end

  def test_empty_stack_clear
    stack = Stack.new
    stack.clear!
    assert stack.empty?
  end

  def test_stack_to_a
    stack = Stack.new [1, 2, 3]
    assert_equal stack.to_a, [1, 2, 3]
  end

  def test_push
    stack = Stack.new
    stack.push! 'ruby'
    assert_equal stack.to_a, %w[ruby]
  end

  def test_pop
    stack = Stack.new [1]
    result = stack.pop!
    assert_equal result, 1
    assert stack.empty?
  end

  def test_pop_empty
    stack = Stack.new
    stack.pop!
    assert stack.empty?
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
