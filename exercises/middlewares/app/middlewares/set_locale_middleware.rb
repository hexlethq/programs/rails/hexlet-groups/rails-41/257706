# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  def initialize(app)
    @app = app
  end

  def call(env)
    locale_from_headers = extract_locale_from_headers(env)
    Rails.logger.debug("Locale in HTTP_ACCEPT_LANGUAGE header: #{locale_from_headers}")

    locale = if locale_is_available?(locale_from_headers)
               Rails.logger.debug("Locale \"#{locale_from_headers}\" from headers is available")
               locale_from_headers
             else
               default_locale = I18n.default_locale
               Rails.logger.debug("Locale from headers is not set or available, using default: #{default_locale}")
               default_locale
             end

    I18n.locale = locale

    status, headers, response = @app.call(env)
    [status, headers, response]
  end

  def extract_locale_from_headers(env)
    env['HTTP_ACCEPT_LANGUAGE']&.scan(/^[a-z]{2}/)&.first
  end

  def locale_is_available?(locale)
    I18n.available_locales.include?((locale || '').to_sym)
  end
  # END
end
