# frozen_string_literal: true

class Router
  def call(env)
    req = Rack::Request.new(env)
    headers = { 'Content-Type' => 'text/plain' }

    case req.path
    when '/about'
      [200, headers, 'About page']
    when '/'
      [200, headers, 'Hello, World!']
    else
      [404, {}, '404 Not Found']
    end
  end
end
