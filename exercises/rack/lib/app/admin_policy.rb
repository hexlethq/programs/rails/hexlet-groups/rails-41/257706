# frozen_string_literal: true

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    req = Rack::Request.new(env)

    if req.path.start_with? '/admin'
      [403, {}, '']
    else
      @app.call(env)
    end
  end
end
