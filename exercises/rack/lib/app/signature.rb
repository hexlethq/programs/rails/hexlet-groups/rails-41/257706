# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, response = @app.call(env)

    hash = Digest::SHA2.hexdigest response

    [status, headers, response + hash]
  end
end
