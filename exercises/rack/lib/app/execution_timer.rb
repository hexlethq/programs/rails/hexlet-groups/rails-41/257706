# frozen_string_literal: true

class ExecutionTimer < Rack::Runtime
  # X-Runtime: 0.001044
end
