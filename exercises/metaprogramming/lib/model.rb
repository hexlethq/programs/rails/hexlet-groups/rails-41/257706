# frozen_string_literal: true

require 'date'

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def attribute(name, options = {})
      item = {
        name: name,
        type: options[:type]
      }

      if class_variable_defined? '@@schema'
        current = class_variable_get '@@schema'
        prepared = current << item
        class_variable_set '@@schema', prepared
      else
        class_variable_set '@@schema', [item]
      end

      define_method name.to_s do
        instance_variable_get "@#{name}"
      end

      define_method "#{name}=" do |value|
        type = self.class.class_variable_get('@@schema').find { |element| element[:name] == name.to_sym }[:type]

        case type
        when :datetime
          day, month, year = value.split('/').map(&:to_i)
          instance_variable_set "@#{name}", DateTime.new(year, month, day)
        when :integer
          instance_variable_set "@#{name}", value.nil? ? nil : value.to_i
        when :string
          instance_variable_set "@#{name}", value.nil? ? nil : value.to_s
        when :boolean
          map = { 'yes' => true, true => true, 'no' => false, false => false }
          instance_variable_set "@#{name}", map[value]
        when nil
          instance_variable_set "@#{name}", value
        else
          raise "unknow type \"#{type}\" for \"#{name}\" attribute"
        end
      end
    end
  end

  def initialize(attributes = {})
    attributes.each do |(key, value)|
      send("#{key}=", value)
    end
  end

  def attributes
    result = {}

    self.class.class_variable_get('@@schema').each do |item|
      name = item[:name]

      result[name] = (instance_variable_get("@#{name}") if instance_variable_defined?("@#{name}"))
    end

    result
  end
end
# END
