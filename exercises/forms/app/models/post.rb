# frozen_string_literal: true

class Post < ApplicationRecord
  validates :title, :body, :summary, presence: true
end
