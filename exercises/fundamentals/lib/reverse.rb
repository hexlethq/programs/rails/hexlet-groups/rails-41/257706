# frozen_string_literal: true

# BEGIN
def last(str)
  str[str.size - 1]
end

def without_last(str)
  str[0...-1]
end

def reverse(str)
  return '' if str.empty?

  last(str) + reverse(without_last(str))
end
# END
