# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num <= 0

  return 0 if num == 1
  return 1 if [2, 3].include?(num)

  fibonacci(num - 1) + fibonacci(num - 2)
end
# END
