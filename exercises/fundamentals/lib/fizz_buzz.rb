# frozen_string_literal: true

# BEGIN
def fizz_buzz_for_number(num)
  result = ''

  result += 'Fizz' if (num % 3).zero?
  result += 'Buzz' if (num % 5).zero?

  result.empty? ? num : result
end

def fizz_buzz(start, stop)
  result = ''

  number = start
  while number <= stop
    result += "#{fizz_buzz_for_number(number)} "
    number += 1
  end

  result.strip
end
# END
