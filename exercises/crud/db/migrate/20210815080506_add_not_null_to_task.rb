# frozen_string_literal: true

class AddNotNullToTask < ActiveRecord::Migration[6.1]
  def change
    change_column :tasks, :completed, :boolean, null: false
  end
end
