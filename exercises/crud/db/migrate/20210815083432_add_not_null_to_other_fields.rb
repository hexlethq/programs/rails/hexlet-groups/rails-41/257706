# frozen_string_literal: true

class AddNotNullToOtherFields < ActiveRecord::Migration[6.1]
  def change
    change_column :tasks, :name, :string, null: false
    change_column :tasks, :status, :string, null: false
    change_column :tasks, :creator, :string, null: false
  end
end
