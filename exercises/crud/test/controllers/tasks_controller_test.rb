# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'should show all tasks' do
    get tasks_path

    assert_response :success
    assert_match 'MyString1', @response.body
    assert_match 'MyString2', @response.body
  end

  test 'should show one task' do
    task = tasks(:one)
    get task_path(task)

    assert_response :success
    assert_match task.name, @response.body
  end

  test 'should show form for creation new task' do
    get new_task_path

    assert_response :success

    assert_match 'Name: ', @response.body
    assert_match 'Description: ', @response.body
    assert_match 'Status: ', @response.body
    assert_match 'Creator: ', @response.body
    assert_match 'Performer: ', @response.body
    assert_match 'Completed: ', @response.body

    assert_match 'Create', @response.body
  end

  test 'should create task' do
    assert_difference('Task.count') do
      post '/tasks', params: { task: {
        name: 'Hello Rails',
        status: 'created',
        creator: 'admin',
        completed: false
      } }
    end

    assert_response :redirect
  end

  test 'should show validation errors for task creation' do
    post tasks_path, params: { task: {
      wrong: 'param'
    } }
    assert_match 'Name can&#39;t be blank', @response.body
    assert_match 'Status can&#39;t be blank', @response.body
    assert_match 'Creator can&#39;t be blank', @response.body
    assert_match 'Completed is not included in the list', @response.body
  end

  test 'should show edit form with filled task data' do
    task = tasks(:one)
    get edit_task_path(task)

    assert_response :success

    assert_match task.name, @response.body
    assert_match task.description, @response.body
    assert_match task.status, @response.body
    assert_match task.creator, @response.body
    assert_match task.performer, @response.body

    assert_match 'Create', @response.body
  end

  test 'should update article' do
    task = tasks(:one)

    put task_url(task), params: { task: {
      name: 'name_updated',
      status: 'status_updated',
      creator: 'creator_updated',
      completed: true
    } }

    assert_redirected_to task_path(task)

    task.reload

    assert_equal 'name_updated', task.name
    assert_equal 'status_updated', task.status
    assert_equal 'creator_updated', task.creator
    assert_equal true, task.completed
  end

  test 'should destroy task' do
    task = tasks(:one)
    assert_difference('Task.count', -1) do
      delete task_url(task)
    end

    assert_redirected_to tasks_path
  end
end
